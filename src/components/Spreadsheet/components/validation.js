/**
 * @Author: 元实
 * @Date:   2023-08-18
 * @Description: 验证规则
 */

/**
 * 验证是否小数
 * @param {string} value 值
 */
export function validateDecimal(value) {
  const regExp = /^-?\d+\.?\d*$/;

  return regExp.test(value);
}

/**
 * 验证是否正小数
 * @param {string} value 值
 */
export function validatePositiveDecimal(value) {
  const regExp = /^\d+\.?\d*$/;

  return regExp.test(value);
}

/**
 * 验证是否整数
 * @param {string} value 值
 */
export function validateInteger(value) {
  const regExp = /^-?\d+$/;

  return regExp.test(value);
}

/**
 * 验证是否正整数
 * @param {string} value 值
 */
export function validatePositiveInteger(value) {
  const regExp = /^\d+$/;

  return regExp.test(value);
}
